﻿using FastColoredTextBoxNS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MachineProgramIncrement {
    public partial class MainForm : Form {
        Style BlueStyle = new TextStyle(Brushes.Blue, null, FontStyle.Bold);
        bool highlightNumbers;

        public MainForm() {
            InitializeComponent();
        }

        void SetInputWay(bool fromFile) {
            if (fromFile) {
                textBox1.Clear();
                textBox1.Enabled = false;
                textBox2.Enabled = true;
            }
            else { 
                textBox1.Clear();
                textBox1.Enabled = true;
                textBox2.Enabled = false;
            }
        }

        void SetOutputWay(bool toFile) {
            if (toFile) {
                textBox4.Clear();
                textBox4.Enabled = false;
                textBox3.Enabled = true;
            } else {
                textBox4.Clear();
                textBox4.Enabled = true;
                textBox3.Enabled = false;
            }
        }

        void HighlightNumbers(bool _is) {
            highlightNumbers = _is;
        }

        void RedrawHighlights(TextChangedEventArgs e) {
            e.ChangedRange.ClearStyle(BlueStyle);
            if (highlightNumbers)
                e.ChangedRange.SetStyle(BlueStyle, @"-?[0-9]+\.?[0-9]{0,}");
        }

        void Process() {
            try {
                var input = textBox1.Text;
                var tokens = (new Lexer()).Tokenize(input);
                //Lexer.DebugTokens(tokens);
                var ast = (new Parser()).Parse(tokens);
                //Parser.DebugAST(ast);
                var gen = new Generator();

                var inc = new Dictionary<string, double>();

                inc.Add("x", (double)numericUpDown1.Value);
                inc.Add("y", (double)numericUpDown2.Value);
                inc.Add("z", (double)numericUpDown3.Value);
                inc.Add("i", (double)numericUpDown4.Value);
                inc.Add("j", (double)numericUpDown5.Value);
                inc.Add("f", (double)numericUpDown6.Value);

                ast = gen.UpdateCoords(ast, inc);
                var res = gen.GenerateString(ast, false);
                //Console.WriteLine("\nRecreated string from AST: \n\n\t" + res);
                textBox4.Text = res;
            }
            catch (Exception e) {
                textBox4.Text = "Во время работы парсера возникла ошибка.\nПроверьте правильность вводимой программы.";
                Console.WriteLine(e);
                Console.WriteLine(e.StackTrace);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e) {
            SetInputWay(true);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e) {
            SetInputWay(false);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e) {
            SetOutputWay(true);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e) {
            SetOutputWay(false);
        }

        private void MainForm_Load(object sender, EventArgs e) {
            SetInputWay(false);
            SetOutputWay(false);
            HighlightNumbers(true);
        }

        private void button2_Click(object sender, EventArgs e) {
            Process();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) {
            HighlightNumbers(checkBox1.Checked);
            textBox1.Text += "";
            textBox4.Text += "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e) {
            RedrawHighlights(e as TextChangedEventArgs);
        }

        private void textBox1_Load(object sender, EventArgs e) {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e) {

        }
    }
}
