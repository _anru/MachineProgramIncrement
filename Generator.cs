﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineProgramIncrement {
    public class Generator {
        public string GenerateString(List<AST.Identifier> ast, bool debug = false) {
            var result = "";
            foreach (var i in ast) {
                var current = "";
                if (debug) current += "[";
                current += i;
                if (debug) current += "]";
                result += current;
            }
            return result;
        }

        public List<AST.Identifier> UpdateCoords(List<AST.Identifier> ast, Dictionary<string, double> incrementTable) {
            foreach (AST.Identifier i in ast) {
                if (!(i is AST.Coord)) continue;
                var currentIncrement = incrementTable.ContainsKey(i.ID.ToLower()) ? incrementTable[i.ID.ToLower()] : 0;
                i.Value = (double)i.Value + currentIncrement;
            }
            return ast;
        }
    }
}
