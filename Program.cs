﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MachineProgramIncrement {

    class Program {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            /*Console.Write("===========================\nInput program: \n\n\t");
            var input = Console.ReadLine();
            if (input == "exit") break;
            var tokens = (new Lexer()).Tokenize(input);
            Lexer.DebugTokens(tokens);
            var ast = (new Parser()).Parse(tokens);
            Parser.DebugAST(ast);
            var gen = new Generator();

            var test = new Dictionary<string, double>();

            test.Add("x", 5);
            test.Add("y", 10);
            test.Add("i", 5);
            test.Add("j", 7);
            test.Add("z", 15);

            ast = gen.UpdateCoords(ast, test);
            Console.WriteLine("\nRecreated string from AST: \n\n\t"+(gen.GenerateString(ast, true)));*/

            // Хак для того, чтобы дробная часть double числа задавались через точку
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
        }
    }
}
