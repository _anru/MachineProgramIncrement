﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineProgramIncrement.AST {
    class Whitespace : Identifier {
        public Whitespace(string value) : base("", value) { }
        public override string ToString() {
            return Value+"";
        }
    }
}
