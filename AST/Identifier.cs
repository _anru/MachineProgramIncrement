﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineProgramIncrement.AST {
    public class Identifier {
        public string ID;
        public object Value;
        public Identifier(string name, object value) {
            ID = name;
            Value = value;
        }
        public override string ToString() {
            return ID + Value;
        }
    }
}
