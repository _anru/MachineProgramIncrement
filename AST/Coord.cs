﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineProgramIncrement.AST {
    public class Coord : Identifier {
        public Coord(string name, string rawvalue) : base(name, Convert.ToDouble(rawvalue)) {}
    }
}
