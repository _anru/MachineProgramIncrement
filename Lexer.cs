﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MachineProgramIncrement {
    public class Lexer {
        public Dictionary<string, Token.TokenType> RegExpTokens = new Dictionary<string, Token.TokenType>();

        public Lexer() {
            RegExpTokens.Add(@"[a-zA-Z]", Token.TokenType.T_Word);
            RegExpTokens.Add(@"-?[0-9]+\.?[0-9]{0,}", Token.TokenType.T_Number);
            RegExpTokens.Add(@"\s+", Token.TokenType.T_Whitespaces);
        }

        public List<Token> Tokenize(string input) {
            var pos = 0;
            var tokens = new List<Token>();
            while (pos < input.Length) {
                Match match = Match.Empty;
                bool found = false;
                foreach (var kv in RegExpTokens) {
                    var rx = new Regex(kv.Key, RegexOptions.IgnoreCase);
                    match = rx.Match(input, pos);
                    if (match.Success && match.Index == pos) {
                        var value = match.Value;
                        found = true;
                        tokens.Add(new Token(kv.Value, value));
                        break;
                    }
                }
                if (found == false) {
                    throw new Exception("Error while lexing. Your input is not correct");
                }
                else {
                    pos += match.Length;
                }
            }
            return tokens;
        }

        public static void DebugTokens(List<Token> tokens) {
            Console.WriteLine("Tokens:\t");
            for (var i = 0; i < tokens.Count; i++) {
                Console.WriteLine(string.Format("[{0}, {1}]", tokens[i].Type.ToString(), tokens[i].Value.ToString()));
            }
        }
    }
}
