﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineProgramIncrement {
    public class Token {
        public enum TokenType {
            T_Word,
            T_Number,
            T_Whitespaces
        }
        public TokenType Type;
        public object Value;

        public Token(TokenType type, object value) {
            Type = type;
            Value = value;
        }
    }
}
