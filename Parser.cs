﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MachineProgramIncrement {
    public class Parser {
        public List<AST.Identifier> Parse(List<Token> input) {
            var result = new List<AST.Identifier>();
            var x = 0;
            while (x < input.Count) {
                // We mean coordinate with name
                if (input[x].Type == Token.TokenType.T_Word) {
                    if (x == input.Count - 1) {
                        break;
                    }
                    if (input[x + 1].Type != Token.TokenType.T_Number) {
                        x += 1;
                        continue;
                    }
                    var val = (string)input[x].Value;
                    if (new Regex(@"[xyzijfXYZIJF]", RegexOptions.IgnoreCase).IsMatch(val)) {
                        result.Add(new AST.Coord((string)input[x].Value, (string)input[x + 1].Value));
                    } else
                        result.Add(new AST.Identifier((string)input[x].Value, (string)input[x + 1].Value));
                    x += 2;
                    continue;
                }
                if (input[x].Type == Token.TokenType.T_Whitespaces) {
                    result.Add(new AST.Whitespace((string)input[x].Value));
                    x += 1;
                    continue;
                }
                x += 1;
            }
            return result;
        }

        public static void DebugAST(List<AST.Identifier> ids) {
            Console.WriteLine("AST:\t");
            for (var i = 0; i < ids.Count; i++) {
                Console.WriteLine(string.Format("[{0}, {1}]", ids[i].GetType(), ids[i].Value.ToString()));
            }
        }
    }
}
